package com.example.Shop.entities.UserRelatedEntities;

public enum UserRole {
    USER,
    ADMIN;
}